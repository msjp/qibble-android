package com.mspratt.qibble;

import com.mspratt.qibble.services.interfaces.ServiceGenerator;

public class AppConstants {

    public static final String PRODUCTION_API = "http://qibbleapi20181225012723.azurewebsites.net/api/v0/";
    public static ServiceGenerator serviceGenerator;
    public static final String applicationIdentifier = "com.mspratt.qibble";
    public static final String baseUrl = "";
    public static final String saveUsername = "saveUser";
    public static final String savePass = "savePass";
    public static final String saveCred = "saveCred";
}
