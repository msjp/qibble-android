package com.mspratt.qibble.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MobileDevice extends BaseModel {

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("user_id")
    @Expose
    public Integer userId;
}
