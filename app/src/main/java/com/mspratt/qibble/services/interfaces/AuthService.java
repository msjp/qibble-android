package com.mspratt.qibble.services.interfaces;

import com.mspratt.qibble.models.DTO.LoginDTO;
import com.mspratt.qibble.models.User;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AuthService {

//    @POST("auth/login")
    @POST("auth/login")
    Observable<String> login(@Body LoginDTO login);

//    @GET("auth/refresh")
//    Observable<Auth> refresh();
}
