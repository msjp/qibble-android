package com.mspratt.qibble.services;

import com.mspratt.qibble.services.interfaces.HeaderInterceptor;
import com.mspratt.qibble.services.interfaces.ServiceHeaders;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.Request;
import okhttp3.Response;

public class RetrofitInterceptor implements HeaderInterceptor {

    private ServiceHeaders headers;

    @Inject
    public RetrofitInterceptor(ServiceHeaders headers) {
        this.headers = headers;
    }

    @Override
    public ServiceHeaders getHeaders() {
        return this.headers;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        final Request.Builder builder = chain.request().newBuilder();
        updateHeaders(builder);

        return chain.proceed(builder.build());
    }

    private void updateHeaders(Request.Builder builder) {
        if (this.headers != null && this.headers.getHeaderMap() != null) {
            Map<String, String> map = this.headers.getHeaderMap();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                builder.header(entry.getKey(), entry.getValue());
            }
        }
    }
}
