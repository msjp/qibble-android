package com.mspratt.qibble.services.interfaces;

import com.mspratt.qibble.models.Message;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface MessageService {

    @GET("message/")
    Observable<List<Message>> getAllMessages();

    @POST("message/")
    Observable<String> createMessage(@Body Message message);

    @DELETE("message/{id}")
    Observable<String> deleteMessage(@Path("id") String id);
}
