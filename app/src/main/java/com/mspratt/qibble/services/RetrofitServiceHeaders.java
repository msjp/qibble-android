package com.mspratt.qibble.services;

import com.mspratt.qibble.services.interfaces.ServiceHeaders;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;

public class RetrofitServiceHeaders implements ServiceHeaders {

    private final Map<String, String> headers = new ConcurrentHashMap<>();

    @Inject
    public RetrofitServiceHeaders() {
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");
    }

    @Override
    public void addHeader(String key, String value) {
        this.headers.put(key, value);
    }

    @Override
    public Map<String, String> getHeaderMap() {
        return this.headers;
    }
}
