package com.mspratt.qibble.services.interfaces;

import com.auth0.android.jwt.JWT;

public interface JWTDecode {

    JWT getDecodedWebToken(String token);
}